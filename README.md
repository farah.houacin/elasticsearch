Farah HOUACIN
M1 IABD 22-24

Exercice 1 :

Que faut-il écrire dans le fichier elasticsearch.yml pour configurer une instance Elasticsearch avec les paramètres suivants :
• Nom du noeud : es-01
• Nom du cluster : magic-system
 

Quel est le nom du fichier à modifier pour paramétrer les options de la JVM ?
Dans le dossier config : « jvm.options »

Quelles est la valeur à écrire pour limiter la heap size à 8GB maximum ?
-Xmx8g

Exercice 2 :

La méthode utilisée : {"index":{"_index": "recettes-test"}}

Endpoint utilisé: POST /_bulk 

POST /_bulk
{"index":{"_index": "recettes-test"}}
{"title": "Tarte aux pommes", "author": "Alice", "description": "Une tarte aux pommes croustillante et délicieuse", "publication_date": "2022-05-01", "pages": 20}
{"index":{"_index": "recettes-test"}}
{"title": "Gaufres au caramel", "author": "Bob", "description": "Des gaufres moelleuses avec une sauce caramel maison", "publication_date": "2021-12-15", "pages": 15}
{"index":{"_index": "recettes-test"}}
{"title": "Muffins au citron", "author": "Dimitri", "description": "Des muffins frais et acidulés", "publication_date": "2023-01-03", "pages": 18}
{"index":{"_index": "recettes-test"}}
{"title": "Crêpes à la framboise", "author": "Zoé", "description": "Des crêpes légères et fruitées", "publication_date": "2022-07-23", "pages": 22}
{"index":{"_index": "recettes-test"}}
{"title": "Tiramisu aux pommes caramélisées", "author": "Camille", "description": "Un tiramisu revisité avec des pommes caramélisées", "publication_date": "2021-11-11", "pages": 25}
{"index":{"_index": "recettes-test"}}
{"title": "Gâteau au citron", "author": "Alex", "description": "Un gâteau moelleux et parfumé au citron", "publication_date": "2023-02-14", "pages": 30}
{"index":{"_index": "recettes-test"}}
{"title": "Tarte aux framboises et citron vert", "author": "Alice", "description": "Une tarte légère et acidulée", "publication_date": "2022-08-09", "pages": 21}
{"index":{"_index": "recettes-test"}}
{"title": "Gaufres à la pomme", "author": "Bob", "description": "Des gaufres aux pommes cuites à la cannelle", "publication_date": "2021-09-30", "pages": 16}
{"index":{"_index": "recettes-test"}}
{"title": "Muffins au caramel", "author": "Dimitri", "description": "Des muffins gourmands au caramel fondant", "publication_date": "2022-04-19", "pages": 19}
{"index":{"_index": "recettes-test"}}
{"title": "Crêpes au citron et sucre", "author": "Zoé", "description": "Des crêpes classiques au citron et sucre", "publication_date": "2023-03-27", "pages": 23}
{"index":{"_index": "recettes-test"}}
{"title": "Gâteau aux framboises", "author": "Camille", "description": "Un gâteau aux framboises légèrement acidulé", "publication_date": "2021-10-10", "pages": 28}
{"index":{"_index": "recettes-test"}}
{"title": "Tarte au citron meringuée", "author": "Alex", "description": "Une tarte au citron avec une belle meringue dorée", "publication_date": "2023-04-01", "pages": 24}
{"index":{"_index": "recettes-test"}}
{"title": "Muffins aux framboises et citron", "author": "Dimitri", "description": "Des muffins moelleux et fruités", "publication_date": "2022-09-16", "pages": 17}
{"index":{"_index": "recettes-test"}}
{"title": "Crêpes au caramel", "author": "Zoé", "description": "Des crêpes avec une sauce caramel maison", "publication_date": "2023-02-28", "pages": 20}
{"index":{"_index": "recettes-test"}}
{"title": "Gaufres à la pomme et cannelle", "author": "Bob", "description": "Des gaufres avec des pommes cuites à la cannelle", "publication_date": "2022-01-07", "pages": 19}
{"index":{"_index": "recettes-test"}}
{"title": "Tiramisu aux framboises", "author": "Alice", "description": "Un tiramisu léger et fruité", "publication_date": "2021-12-25", "pages": 26}
{"index":{"_index": "recettes-test"}}
{"title": "Gâteau au citron et aux amandes", "author": "Camille", "description": "Un gâteau moelleux et parfumé aux amandes et au citron", "publication_date": "2022-10-03", "pages": 27}
{"index":{"_index": "recettes-test"}}
{"title": "Tarte aux pommes et à la cannelle", "author": "Alex", "description": "Une tarte aux pommes avec de la cannelle et une pâte sablée croquante", "publication_date": "2023-03-10", "pages": 23}
{"index":{"_index": "recettes-test"}}
{"title": "Muffins au caramel et noisettes", "author": "Dimitri", "description": "Des muffins gourmands avec un cœur de caramel et des éclats de noisettes", "publication_date": "2022-05-14", "pages": 22}
{"index":{"_index": "recettes-test"}}
{"title": "Crêpes à la pomme et cannelle", "author": "Zoé", "description": "Des crêpes avec des pommes cuites à la cannelle et une pointe de vanille", "publication_date": "2023-01-20", "pages": 21}

Exercice 3 :

1.  Rechercher toutes le recettes qui contiennent “pomme” (au singulier) dans leur description. 
a.  Avez-vous des résultats ? Si oui lesquels, sinon que faudrait-il faire pour en avoir ?

Non, nous n’avons pas de résultats. Le french analyzer permet de faire des recherches sans prendre en compte les formes de conjugaisons grammaticales comme le « s » du pluriel.

GET /recettes-test/_search {
  "query": {
    "match": {
      "description": "pomme"
    }
  }
}

2.  Rechercher les recettes de “Zoé” dont le nombre de pages est strictement inférieur à 21. 
b.  Combien de documents avez-vous obtenu ?

Nous obtenons un document:

GET /recettes-test/_search
{
  "query": {
    "bool": {
      "must": [
        {"match": {
          "author.keyword": "Zoé"
        }},
        {"range": {
          "pages": {
            "lt": 21
          }
        }}
      ]
    }
  }
}



Exercice 4 :

Mettre à jour le mapping pour rajouter un champ “theme” à l’index “recettes-test”.
Quel serait le type de champ approprié ?

Le type de champs keyword est approprié (non analysé, pour la recherche exacte).

PUT /recettes-test/_mapping
{
  "properties": {
    "theme": {
      "type": "keyword"
    }
  }
}

Exercice 5 :

Création de l'index et du mapping :

PUT /recettes-test-mapping
{
  "mappings": {
    "properties": {
      "titre": {"type": "text"},
      "author": {"type": "keyword"},
      "description": {"type": "text"},
      "publication_date": {"type": "date"},
      "pages": {"type": "integer"}
    }
  }
}

Réindexation :

POST _reindex
{
  "source": {
    "index": "recettes-test"
  },
  "dest": {
    "index": "recettes-test-mapping"
  }
}

Exercice 6 :

1.  Dénombrer le nombre de recettes par auteur, écrites avant 2023.

GET /recettes-test/_search
{
  "size": 20, 
  "query": {
    "range": {
      "publication_date": {
        "lt": "2023"
      }
    }
  },
  "aggs": {
    "recette-par-auteur": {
      "terms": {
        "field": "author.keyword"
      }
    }
  }
}

2.  Trouver les moyennes de nombre de pages des recettes de par auteur.

GET /recettes-test/_search
{
  "size": 0,
  "aggs": {
    "aggregation-utilisateur": {
      "terms": {
        "field": "author.keyword"
      },
      "aggs": {
        "moyenne-page": {
          "avg": {
            "field": "pages"
          }
        }
      }
    }
  }
}

Exercice 7 :

Création d'un nouvel index :

PUT /recettes-fr
{
  "settings": {
    "analysis": {
      "analyzer": {
        "default": {
          "type": "french"
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "titre": {"type": "text"},
      "author": {"type": "keyword"},
      "description": {"type": "text"},
      "publication_date": {"type": "date"},
      "pages": {"type": "integer"}
    }
  }
}

Réindexion des documents dans le nouvel index :

POST _reindex
{
  "source": {
    "index": "recettes-test"
  },
  "dest": {
    "index": "recettes-fr"
  }
}

Exercice 8 :

1. Indexer un document dans l’index “**recettes-fr**” avec un nombre de pages négatif. 
Est-ce que le document a pu être indexé ?

Oui car le type integer reconnait les nombres négatifs.

Indexation du nouveau document avec un nombre de pages négatif :

POST /recettes-fr/_doc
{
  "title": "Crêpes au nutella",
  "author": "Zoé",
  "description": "Des crêpes au nutella",
  "publication_date": "2022-07-22",
  "pages": -5
}

2. Rechercher les documents dans "**recettes-fr**” qui contiennent “**pomme**” dans leur description.
Combien de documents sont renvoyés ?

6 documents sont renvoyés.

Récupération des documents contenant le mot pomme dans leurs descriptions :

GET /recettes-fr/_search
{
  "query": {
    "match": {
      "description": "pomme"
    }
  }
}

Exercice 9 :

Quels sont les tokens générés par l’analyzer norvégien de la phrase suivante ?

-> Jeg elsker klokka åtte-klasser

POST _analyze
{
  "analyzer": "norwegian",
  "text": "Jeg elsker klokka åtte-timer"
}
{
  "tokens": [
    {
      "token": "elsk",
      "start_offset": 4,
      "end_offset": 10,
      "type": "<ALPHANUM>",
      "position": 1
    },
    {
      "token": "klokk",
      "start_offset": 11,
      "end_offset": 17,
      "type": "<ALPHANUM>",
      "position": 2
    },
    {
      "token": "ått",
      "start_offset": 18,
      "end_offset": 22,
      "type": "<ALPHANUM>",
      "position": 3
    },
    {
      "token": "tim",
      "start_offset": 23,
      "end_offset": 28,
      "type": "<ALPHANUM>",
      "position": 4
    }
  ]
}

Exercice 10 :

1. Définir un index template qui appliquera :
    1. L’alias “**recettes**” à tous les index avec le pattern de nommage “**recettes-***”
    2. Le mapping de la question 5
    3. L’analyzer french par défaut

PUT _index_template/template-index
{
  "index_patterns": "recettes-*",
  "template": {
    "settings": {
      "analysis": {
        "analyzer": {
          "default": {
            "type": "french"
          }
        }
      }
    },
    "mappings": {
      "properties": {
        "titre": {
          "type": "text"
        },
        "author": {
          "type": "keyword"
        },
        "description": {
          "type": "text"
        },
        "publication_date": {
          "type": "date"
        },
        "pages": {
          "type": "integer"
        }
      }
    },
    "aliases": {
      "recettes": {}
    }
  }
}


2.
Indexer un document dans l’index “recettes-es” sans le créer au préalable.

POST /recettes-es/_doc
{
  "title": "Crêpes",
  "author": "Farah",
  "description": "Des crêpes",
  "publication_date": "2023-03-23",
  "pages": 5
}


3.
Vérifier que l’index template a fonctionné.

GET /recettes-es

{
  "recettes-es": {
    "aliases": {
      "recettes": {}
    },
    "mappings": {
      "properties": {
        "author": {
          "type": "keyword"
        },
        "description": {
          "type": "text"
        },
        "pages": {
          "type": "integer"
        },
        "publication_date": {
          "type": "date"
        },
        "title": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "titre": {
          "type": "text"
        }
      }
    },
    "settings": {
      "index": {
        "routing": {
          "allocation": {
            "include": {
              "_tier_preference": "data_content"
            }
          }
        },
        "number_of_shards": "1",
        "provided_name": "recettes-es",
        "creation_date": "1682594715573",
        "analysis": {
          "analyzer": {
            "default": {
              "type": "french"
            }
          }
        },
        "number_of_replicas": "1",
        "uuid": "9iqNOeM0QyyjmFMbFSIU4Q",
        "version": {
          "created": "8070099"
        }
      }
    }
  }
}


4.
Compter le nombre de documents derrière l’alias “recettes”.
--> 1

GET /recettes
{
  "took": 0,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 1,
      "relation": "eq"
    },
    "max_score": 1,
    "hits": [
      {
        "_index": "recettes-es",
        "_id": "rfl1wocBT1ZecB6g2FAM",
        "_score": 1,
        "_source": {
          "title": "Crêpes",
          "author": "Farah",
          "description": "Des crêpes",
          "publication_date": "2023-03-23",
          "pages": 5
        }
      }
    ]
  }
}


